package top.cwk.model.customerPo;

import java.io.Serializable;

public class Customer implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer uid;
	private String username;
	private String password;
	private String phone;
	
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Customer(Integer uid, String username, String password, String phone) {
		super();
		this.uid = uid;
		this.username = username;
		this.password = password;
		this.phone = phone;
	}
	public Customer(){}
	
	@Override
	public String toString() {
		return "Customer [uid=" + uid + ", username=" + username
				+ ", password=" + password + ", phone=" + phone + "]";
	}
	
	
}
