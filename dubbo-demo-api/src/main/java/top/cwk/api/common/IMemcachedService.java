package top.cwk.api.common;

import java.util.Map;

public interface IMemcachedService {

	/**
	 * 
	 * Description:添加一个不过期的SessionMap
	 * @author: 陈维康
	 * @date: 2017年8月25日
	 * @param key
	 * @param SessionMap  
	 * @return boolean
	 */
	boolean addForeverSessionMap(String key, Map<String,String> SessionMap);
	
	/**
	 * 
	 * Description:从memcached中获取对象
	 * @author: 陈维康
	 * @date: 2017年8月25日
	 * @param key
	 * @return Object
	 */
	Object get(String key);
}
