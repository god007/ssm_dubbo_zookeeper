package top.cwk.api.customer;

import java.util.List;

import top.cwk.model.customerPo.Customer;

public interface ICustomerService {

	public void sayHello();
	
	public Customer getOneCustomer(Integer uid);
	
	public List<Customer> getCustomerList();
	
	public Customer getCustomerLogin(String username,String password);
	
}
