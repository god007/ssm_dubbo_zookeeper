package top.cwk.core.customer.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.cwk.api.customer.ICustomerService;
import top.cwk.core.customer.mapper.CustomerMapper;
import top.cwk.model.customerPo.Customer;

@Service("customerService")
public class CustomerServlceImpl implements ICustomerService{

	@Autowired
	private CustomerMapper customerMapper;
	
	public void sayHello() {
		System.out.println("hello dubbo service...");
	}

	@Override
	public Customer getOneCustomer(Integer uid) {
		return customerMapper.selectByUid(uid);
	}

	@Override
	public List<Customer> getCustomerList() {
		return customerMapper.selectAll();
	}
	

	/**
	 * 
	 * Description:用户登录
	 * @author: 陈维康
	 * @date: 2017年8月25日
	 * @param username
	 * @param password
	 * @return
	 */
	public Customer getCustomerLogin(String username,String password) {
		return customerMapper.selectByUsernameAndPassword(username,password);
	}
	
}
