package top.cwk.core.customer.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import top.cwk.model.customerPo.Customer;

public interface CustomerMapper {

	 Customer selectByUid(Integer uid);
	
	 List<Customer> selectAll();
	
	 Customer selectByUsernameAndPassword(@Param("username") String username, 
			 @Param("password") String password);
}
