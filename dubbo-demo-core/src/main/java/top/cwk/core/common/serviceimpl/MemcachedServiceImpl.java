package top.cwk.core.common.serviceimpl;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.whalin.MemCached.MemCachedClient;

import top.cwk.api.common.IMemcachedService;

@Service("mcachedService")
public class MemcachedServiceImpl implements IMemcachedService{

	@Autowired@Qualifier("memcachedClient")
	private MemCachedClient cachedClient;  
	
	private Logger logger = Logger.getLogger(MemcachedServiceImpl.class);
	
	@Override
	public boolean addForeverSessionMap(String key,
			Map<String, String> SessionMap) {
			return setExp(key , SessionMap ,new Date(0));
	}

	@Override
	public Object get(String key) {
		return cachedClient.get(key);
	}
	
	private boolean setExp(String key, Object value, Date expire) {  
		boolean flag = false;  
		try {  
			flag = cachedClient.set(key, value, expire);  
		} catch (Exception e) {  
			logger.error("Memcached ："+e);  
		}  
		return flag;  
	}  

}
