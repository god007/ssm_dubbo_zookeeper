### SSM+Dubbo+Zookeeper整合的分布式项目

#### 目录结构
- database -- 数据库备份
- dubbo-demo-api -- 接口模块
- dubbo-demo-model -- 实体类模块
- dubbo-demo-core -- 服务层
- dubbo-demo-client -- 客户端

#### 运行环境
- Tomcat7
- JDK1.7
- Zookeeper3.4.6

#### 启动前先运行zookeeper
- 下载地址：https://mirrors.tuna.tsinghua.edu.cn/apache/zookeeper/zookeeper-3.4.6/