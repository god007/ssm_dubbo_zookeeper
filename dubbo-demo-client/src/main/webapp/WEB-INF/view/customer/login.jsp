<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="path" value="${pageContext.request.contextPath}"/>
<html>
<head>
	<title>用户登录</title>
	<script type="text/javascript" src="${path}/assets/js/jquery.js"></script>
	<style>
		.hide{
			display: none;
		}
	</style>
</head>
	<body>
		<h3>用户登录</h3>
		<p style="color:red" class="hide" id="forceLogout">该账户另一地点已登录，您已被强制下线!</p>
		<div class="loginForm">
			<form val="${path}/customer/login" action="?" method="post"  id="loginForm">
				<input type="hidden" name="isForceLogin" id="isForceLogin" />
				<p>username:<input type="text"  name="username"/></p>
				<p>password:<input type="text"  name="password"/></p>
				<p><button type="button" onclick="checkLogin()" id="loginButton">登录</button></p>
			</form>
		</div>
		<script>
			
		$(function(){
			if((location.href).indexOf("forceLogout")!=-1){
				$("#forceLogout").show("200");
			}
		});
			function checkLogin(){
				$.ajax({
					type:"post",
					url:$("#loginForm").attr("val"),
					data:$("#loginForm").serialize(),
					success:function(code){
						if(code=="0000"){
							location.href="${path}/customer/welcome";
						}else if(code=="0001"){
							$("#isForceLogin").val();
							alert("用户名或密码错误！");
						}else if(code=="0002"){
							var mssage = confirm("用户在另一地点已登录，是否强制登录？");
							if(mssage){
								$("#isForceLogin").val("true");
								$("#loginButton").click();
							}
							return false;
						}
					},
					error:function(err , status){
						
					}
				});
				
				return false;
			}
		
		</script>
	</body>
</html>