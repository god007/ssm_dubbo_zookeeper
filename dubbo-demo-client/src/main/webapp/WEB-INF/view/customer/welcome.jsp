<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="path" value="${pageContext.request.contextPath}"/>
<html>
<head>
	<title>Welcome</title>
</head>
	<body>
		<h4><strong>${customer.username }</strong> 欢迎回来 | 
		<a href="${path}/customer/logout">退出登录</a></h4>
		<p><strong>password:</strong>${customer.password }</p>
		<p><strong>phone:</strong>${customer.phone }</p>
	</body>
</html>