package top.cwk.controller.customer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import top.cwk.api.customer.ICustomerService;
import top.cwk.listener.SessionListener;
import top.cwk.model.customerPo.Customer;


@Controller
public class CustomerController {

	@Autowired
	private ICustomerService customerService;
	
	@RequestMapping("/sayHello")
	public String toHello(){
		customerService.sayHello();
		return "/index";
	}
	
	@RequestMapping("/toIndex")
	public String toIndex(){
		return "/index";
	}
	
	@RequestMapping("/cus-info")
	@ResponseBody
	public Customer cusInfo(){
		return customerService.getOneCustomer(1);
	}
	
	@RequestMapping("/cus-all")
	@ResponseBody
	public List<Customer> cusAll(){
		return customerService.getCustomerList();
	}
	
	@RequestMapping("/customer/toLogin")
	public String toLogin(){
		return "/customer/login";
	}
	
	@RequestMapping("/customer/welcome")
	public String welcome(){
		return "/customer/welcome";
	}
	
	@RequestMapping("/customer/logout")
	public String logout(HttpServletRequest request){
		Customer customer = (Customer) request.getSession().getAttribute("customer");
		if(customer!=null){
			SessionListener.removeUserSession(customer.getUsername());
			request.getSession().removeAttribute("customer");
		}
		return "/customer/login";
	}
	
	@RequestMapping("/customer/clearSessionMap")
	public String clearSessionMap(){
		SessionListener.clearSessionList();
		return "redirect:/customer/toLogin";
	}
	
	@RequestMapping("/customer/login")
	@ResponseBody
	public String login(HttpServletRequest request ){
		String isForceLogin = request.getParameter("isForceLogin");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if(isForceLogin!=null && "true".equals(isForceLogin)){
			//强制登录
			Customer customer = customerService.getCustomerLogin(username, password);
			if(customer!=null){
				SessionListener.replaceLogin(request.getSession(), username);
				request.getSession().setAttribute("customer", customer);
				return "0000";
			}else{
				return "0001"; //用户名或密码错误
			}
		}
		
		if(SessionListener.isContainsSessionID(request.getSession().getId())){
			//已登录
			Customer customer= new Customer(100,username,password,"15617888360");
			request.getSession().setAttribute("customer", customer);
			return "0000";
		}else{
			if(SessionListener.isContainsUsername(username)){
				//用户已登录，请强制登录
				return "0002";
			}else{
				//重新校验
				Customer customer = customerService.getCustomerLogin(username, password);
				if(customer!=null){
					request.getSession().setAttribute("customer", customer);
					SessionListener.createUserSession(request.getSession(), username);
					return "0000";
				}else{
					return "0001"; //用户名或密码错误
				}
			}
		}
	}
	
	
	
}
